var BISHOP = (function() {
  function Bishop(color, pos) {
    this.color = color;
    this.position = pos;
    this.__name = "bishop";
  }

  Bishop.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];
    var currentX = x,
      currentY = y;

    if (x > 0 && y > 0) {
      // Top left
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY--;
        currentX--;

        if (currentY < 0 || currentX < 0) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x < 7 && y > 0) {
      // Top right
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY--;
        currentX++;

        if (currentY < 0 || currentX > 7) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x < 7 && y < 7) {
      // Bottom right
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY++;
        currentX++;

        if (currentY > 7 || currentX > 7) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x > 0 && y < 7) {
      // Bottom left
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY++;
        currentX--;

        if (currentY > 7 || currentX < 0) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }
  };

  Bishop.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];
    var range = x2 - x1;
    range = -range > 0 ? -range : range;

    if (x2 - x1 === y2 - y1 || x1 + y1 === x2 + y2) {
      // Detect collision
      if (range > 1) {
        var x = x1,
          y = y1;

        for (var i = 1; i < range; i++) {
          x = x2 > x1 ? x + 1 : x - 1;
          y = y2 > y1 ? y + 1 : y - 1;

          if (board._fields[y][x] !== 0) {
            console.log("Collision detected!");
            return false;
          }
        }
      }

      // Just move
      board.move(this.position, pos);
    } else {
      // Illegal movedirection
      console.log("Illegal move direction!");
      return false;
    }
  };

  function init(color, pos) {
    console.log("Init bishop...");
    var bishop = new Bishop(color, pos);
    return bishop;
  }

  return {
    init: init
  };
})();
