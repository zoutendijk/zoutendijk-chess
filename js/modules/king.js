var KING = (function() {
  function King(color, pos) {
    this.color = color;
    this.isChecked = false;
    this.position = pos;
    this.hasMoved = false;
    this.attackers = [];
    this.__name = "king";
  }

  King.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];

    // Castling
    if (this.hasMoved === false && this.isChecked === false) {
      // Castling short (white)
      if (
        x2 === 6 &&
        y2 === 7 &&
        !board._defendedFields[7][5][1].length &&
        !board._defendedFields[7][6][1].length &&
        board._fields[7][5] === 0 &&
        board._fields[7][6] === 0 &&
        board._fields[7][7].__name === "rook" &&
        board._fields[7][7].hasMoved === false
      ) {
        board._fields[7][7].position = [7, 5];
        board._fields[7][5] = board._fields[7][7];
        board._fields[7][7] = 0;
        this.hasMoved = true;
        board.move(this.position, pos);
        return true;
      }

      // Castling long (white)
      if (
        x2 === 2 &&
        y2 === 7 &&
        !board._defendedFields[7][1][1].length &&
        !board._defendedFields[7][2][1].length &&
        !board._defendedFields[7][3][1].length &&
        board._fields[7][1] === 0 &&
        board._fields[7][2] === 0 &&
        board._fields[7][3] === 0 &&
        board._fields[7][0].__name === "rook" &&
        board._fields[7][0].hasMoved === false
      ) {
        board._fields[7][0].position = [7, 3];
        board._fields[7][3] = board._fields[7][0];
        board._fields[7][0] = 0;
        this.hasMoved = true;
        board.move(this.position, pos);
        return true;
      }

      // Castling short (black)
      if (
        x2 === 6 &&
        y2 === 0 &&
        !board._defendedFields[0][5][0].length &&
        !board._defendedFields[0][6][0].length &&
        board._fields[0][5] === 0 &&
        board._fields[0][6] === 0 &&
        board._fields[0][7].__name === "rook" &&
        board._fields[0][7].hasMoved === false
      ) {
        board._fields[0][7].position = [0, 5];
        board._fields[0][5] = board._fields[0][7];
        board._fields[0][7] = 0;
        this.hasMoved = true;
        board.move(this.position, pos);
        return true;
      }

      // Castling long (black)
      if (
        x2 === 2 &&
        y2 === 0 &&
        !board._defendedFields[0][1][0].length &&
        !board._defendedFields[0][2][0].length &&
        !board._defendedFields[0][3][0].length &&
        board._fields[0][1] === 0 &&
        board._fields[0][2] === 0 &&
        board._fields[0][3] === 0 &&
        board._fields[0][0].__name === "rook" &&
        board._fields[0][0].hasMoved === false
      ) {
        board._fields[0][0].position = [0, 3];
        board._fields[0][3] = board._fields[0][0];
        board._fields[0][0] = 0;
        this.hasMoved = true;
        board.move(this.position, pos);
        return true;
      }
    }

    // Illegal movedirection
    if (x2 > x1 + 1 || x2 < x1 - 1 || y2 > y1 + 1 || y2 < y1 - 1) {
      console.log("Illegal move direction!");
      return false;
    }

    // Just move
    if (board.move(this.position, pos)) {
      this.hasMoved = true;
    }
  };

  King.prototype.canMoveToSafety = function() {
    var x = this.position[1],
      y = this.position[0];

    var moves = [
      [y - 1, x],
      [y - 1, x + 1],
      [y - 1, x - 1],
      [y + 1, x],
      [y + 1, x + 1],
      [y + 1, x - 1],
      [y, x + 1],
      [y, x - 1]
    ];

    for (var i = 0; i < moves.length; i++) {
      if (
        moves[i][0] >= 0 &&
        moves[i][1] >= 0 &&
        moves[i][0] <= 7 &&
        moves[i][1] <= 7
      ) {
        var fieldToCheck = board._fields[moves[i][0]][moves[i][1]];

        if (fieldToCheck === 0 || fieldToCheck.color !== this.color) {
          if (board.checkChecks([y, x], moves[i], this.color)) {
            return true;
          }
        }
      }
    }

    console.log("can't move to safety");
    return false;
  };

  King.prototype.isMate = function() {
    var activePlayer = this.color === "white" ? 0 : 1;
    var x = this.position[1],
      y = this.position[0];

    // Can King move to safety?
    if (this.canMoveToSafety()) {
      return false;
    } else {
      if (this.attackers.length > 1) {
        return true;
      }
    }

    // Can attacker be captured?
    var attacker = this.attackers[0];
    var attackerY = attacker.position[0];
    var attackerX = attacker.position[1];
    var defendersActivePlayer =
      board._defendedFields[attackerY][attackerX][activePlayer];

    for (var i = 0; i < defendersActivePlayer.length; i++) {
      var defender = defendersActivePlayer[i];

      // Is defender not pinned?
      if (
        board.checkChecks(
          [defender.position[0], defender.position[1]],
          [attackerY, attackerX],
          this.color
        )
      ) {
        console.log("Attacker can be captured");
        return false;
      }
    }

    // Can piece be placed in between King and attacker?
    if (
      attacker.__name === "rook" ||
      attacker.__name === "queen" ||
      attacker.__name === "bishop"
    ) {
      var directionY = attackerY === y ? 0 : attackerY > y ? -1 : 1;
      var directionX = attackerX === x ? 0 : attackerX > x ? -1 : 1;

      var diff = directionY === 0 ? attackerX - x : attackerY - y;
      diff = -diff > 0 ? -diff : diff;
      diff--;

      for (var i = 1; i <= diff; i++) {
        var fieldX = directionX === 0 ? attackerX : attackerX + i * directionX;
        var fieldY = directionY === 0 ? attackerY : attackerY + i * directionY;
        var betweners = board._defendedFields[fieldY][fieldX][activePlayer];

        for (var j = 0; j < betweners.length; j++) {
          var betwener = betweners[j];

          if (betwener.__name !== "pawn") {
            // Pawns defending fields are not in-between fields
            // Is betwener not pinned?
            if (
              board.checkChecks(
                [betwener.position[0], betwener.position[1]],
                [fieldY, fieldX],
                this.color
              )
            ) {
              console.log("Piece can be placed in between", betwener);
              return false;
            }
          }
        }

        // Now we will check if we can place pawn in between
        var pawnDirection = this.color === "white" ? -1 : 1;
        var fieldYPawn = fieldY - 1 * pawnDirection;
        var fieldYPawn2 = fieldY - 2 * pawnDirection;

        var pawnField1 =
          fieldYPawn >= 0 && fieldYPawn <= 7
            ? board._fields[fieldYPawn][fieldX]
            : 0;
        var pawnField2 =
          fieldYPawn2 >= 0 && fieldYPawn2 <= 7
            ? board._fields[fieldYPawn2][fieldX]
            : 0;

        if (
          pawnField1 !== 0 &&
          pawnField1.__name === "pawn" &&
          pawnField1.color === this.color
        ) {
          if (
            board.checkChecks(
              [pawnField1.position[0], pawnField1.position[1]],
              [fieldY, fieldX],
              this.color
            )
          ) {
            console.log("Pawn can be placed in between", pawnField1);
            return false;
          }
        }

        if (
          pawnField2 !== 0 &&
          pawnField2.__name === "pawn" &&
          pawnField2.color === this.color &&
          pawnField2.canMoveTwo &&
          pawnField1 === 0
        ) {
          if (
            board.checkChecks(
              [pawnField2.position[0], pawnField2.position[1]],
              [fieldY, fieldX],
              this.color
            )
          ) {
            console.log("Pawn can be placed in between", pawnField2);
            return false;
          }
        }
      }
    }

    return true;
  };

  King.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];
    var moves = [
      [y - 1, x],
      [y - 1, x + 1],
      [y - 1, x - 1],
      [y + 1, x],
      [y + 1, x + 1],
      [y + 1, x - 1],
      [y, x + 1],
      [y, x - 1]
    ];

    for (var i = 0; i < moves.length; i++) {
      if (
        moves[i][0] >= 0 &&
        moves[i][1] >= 0 &&
        moves[i][0] <= 7 &&
        moves[i][1] <= 7
      ) {
        var fieldToCalculate = [moves[i][0], moves[i][1]];

        board.calculateDefendedField(fieldToCalculate, this);
      }
    }
  };

  function init(color, pos) {
    console.log("Init king...");
    var king = new King(color, pos);
    return king;
  }

  return {
    init: init
  };
})();
