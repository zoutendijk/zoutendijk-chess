var KNIGHT = (function() {
  function Knight(color, pos) {
    this.color = color;
    this.position = pos;
    this.__name = "knight";
  }

  Knight.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];

    if (y - 2 >= 0) {
      if (x + 1 <= 7) {
        board.calculateDefendedField([y - 2, x + 1], this);
      }
      if (x - 1 >= 0) {
        board.calculateDefendedField([y - 2, x - 1], this);
      }
    }

    if (y + 2 <= 7) {
      if (x + 1 <= 7) {
        board.calculateDefendedField([y + 2, x + 1], this);
      }
      if (x - 1 >= 0) {
        board.calculateDefendedField([y + 2, x - 1], this);
      }
    }

    if (x - 2 >= 0) {
      if (y + 1 <= 7) {
        board.calculateDefendedField([y + 1, x - 2], this);
      }
      if (y - 1 >= 0) {
        board.calculateDefendedField([y - 1, x - 2], this);
      }
    }

    if (x + 2 <= 7) {
      if (y + 1 <= 7) {
        board.calculateDefendedField([y + 1, x + 2], this);
      }
      if (y - 1 >= 0) {
        board.calculateDefendedField([y - 1, x + 2], this);
      }
    }
  };

  Knight.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];

    if (x2 === x1 + 1 || x2 === x1 - 1) {
      if (y2 === y1 + 2 || y2 === y1 - 2) {
        board.move(this.position, pos);
      }
    } else {
      if (x2 === x1 + 2 || x2 === x1 - 2) {
        if (y2 === y1 + 1 || y2 === y1 - 1) {
          board.move(this.position, pos);
        }
      }
    }
  };

  function init(color, pos) {
    console.log("Init knight...");
    var knight = new Knight(color, pos);
    return knight;
  }

  return {
    init: init
  };
})();
