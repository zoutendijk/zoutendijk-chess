// 50 zetten regel (stuk geslagen of pion bewogen)
// Remise meten
// Pat
// En passant mat blokkeren

var BOARD = (function() {
  function Board() {
    this._fields = [
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0]
    ];

    this._defendedFields = [
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ]
    ];

    this.capturedPieces = [];
    this.activePlayer = "white";
    this.lastMovedPiece = null;
  }

  Board.prototype.calculateDefendedField = function(pos, elem) {
    var color = elem.color;
    var field = this._fields[pos[0]][pos[1]];
    var defendedField = this._defendedFields[pos[0]][pos[1]];

    if (color === "white") {
      defendedField[0].push(elem);
    } else {
      defendedField[1].push(elem);
    }

    if (field !== 0) {
      if (field.__name === "king" && field.color !== color) {
        field.isChecked = true;
        field.attackers.push(elem);
      }

      return true;
    } else {
      return false;
    }
  };

  Board.prototype.resetPieces = function() {
    // // Kings
    whiteKing = KING.init("white", [7, 4]);
    blackKing = KING.init("black", [0, 4]);

    this._fields[7][4] = whiteKing;
    this._fields[0][4] = blackKing;

    // this._fields[1][7] = QUEEN.init("white", [1,7]);
    // this._fields[3][5] = KNIGHT.init("white", [3,5]);

    // Rooks
    this._fields[7][0] = ROOK.init("white", [7, 0]);
    this._fields[7][7] = ROOK.init("white", [7, 7]);

    this._fields[0][0] = ROOK.init("black", [0, 0]);
    this._fields[0][7] = ROOK.init("black", [0, 7]);

    // Bishops
    this._fields[7][2] = BISHOP.init("white", [7, 2]);
    this._fields[7][5] = BISHOP.init("white", [7, 5]);

    this._fields[0][2] = BISHOP.init("black", [0, 2]);
    this._fields[0][5] = BISHOP.init("black", [0, 5]);

    // Knights
    this._fields[7][1] = KNIGHT.init("white", [7, 1]);
    this._fields[7][6] = KNIGHT.init("white", [7, 6]);

    this._fields[0][1] = KNIGHT.init("black", [0, 1]);
    this._fields[0][6] = KNIGHT.init("black", [0, 6]);

    // Queens
    this._fields[7][3] = QUEEN.init("white", [7, 3]);
    this._fields[0][3] = QUEEN.init("black", [0, 3]);

    // Pawns
    for (var i = 0; i <= 7; i++) {
      this._fields[6][i] = PAWN.init("white", [6, i]);
      this._fields[1][i] = PAWN.init("black", [1, i]);
    }

    board.paintBoard();
  };

  Board.prototype.passTurn = function() {
    var element = document.getElementById("current-player");
    this.activePlayer = this.activePlayer === "white" ? "black" : "white";

    element.innerHTML = this.activePlayer;
  };

  Board.prototype.isLegalPiece = function(pos) {
    var current = this._fields[pos[0]][pos[1]];

    // Piece of other color or empty square
    if (current.color !== this.activePlayer || current === 0) {
      return false;
    }

    return true;
  };

  Board.prototype.checkDefendedFields = function() {
    // First reset defendedFields array
    this._defendedFields = [
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ],
      [
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []],
        [[], []]
      ]
    ];

    // Reset checks
    blackKing.isChecked = false;
    whiteKing.isChecked = false;

    // Reset attackers
    blackKing.attackers = [];
    whiteKing.attackers = [];

    for (var i = 0; i < 8; i++) {
      for (var j = 0; j < 8; j++) {
        if (this._fields[i][j] !== 0) {
          this._fields[i][j].calculateDefendedFields();
        }
      }
    }

    return true;
  };

  Board.prototype.paintBoard = function() {
    element = document.getElementById("board");
    html = "";

    for (var i = 0; i < 8; i++) {
      html += '<div class="row">';

      for (var j = 0; j < 8; j++) {
        var className = this._fields[i][j].color
          ? " " + this._fields[i][j].color + " " + this._fields[i][j].__name
          : "";
        html +=
          '<div data-x="' + j + '" data-y="' + i + '" class="cell' + className;
        html += '"></div>';
      }
      html += "</div>";
    }

    element.innerHTML = html;

    this.addCellListeners();
  };

  Board.prototype.paintCapturedPieces = function() {
    element = document.getElementById("captured-pieces");
    html = '<div class="captured-pieces__white">';

    for (var i = 0; i < this.capturedPieces.length; i++) {
      if (this.capturedPieces[i].color === "white") {
        html +=
          '<div class="captured-piece ' +
          this.capturedPieces[i].__name +
          " " +
          this.capturedPieces[i].color +
          '"></div>';
      }
    }

    html += '</div><div class="captured-pieces__black">';
    for (var i = 0; i < this.capturedPieces.length; i++) {
      if (this.capturedPieces[i].color === "black") {
        html +=
          '<div class="captured-piece ' +
          this.capturedPieces[i].__name +
          " " +
          this.capturedPieces[i].color +
          '"></div>';
      }
    }
    html += "</div>";

    element.innerHTML = html;
  };

  Board.prototype.checkChecks = function(pos1, pos2, player) {
    var current = this._fields[pos1[0]][pos1[1]];
    var target = this._fields[pos2[0]][pos2[1]];
    var legalMove = true;
    var player = player ? player : this.activePlayer;

    var currentKingToCheck = player === "white" ? whiteKing : blackKing;

    // Change board state
    this._fields[pos2[0]][pos2[1]] = current;
    this._fields[pos1[0]][pos1[1]] = 0;
    current.position = pos2;

    // Check for checks
    if (this.checkDefendedFields()) {
      if (currentKingToCheck.isChecked === true) {
        legalMove = false;
      }
    }

    // Set back previous positions
    this._fields[pos1[0]][pos1[1]] = current;
    this._fields[pos2[0]][pos2[1]] = target;
    current.position = pos1;

    // Reset defendedFields, checks and attackers
    this.checkDefendedFields();

    return legalMove;
  };

  Board.prototype.move = function(pos1, pos2) {
    var current = this._fields[pos1[0]][pos1[1]];
    var target = this._fields[pos2[0]][pos2[1]];

    // Check for checks
    if (!this.checkChecks(pos1, pos2)) {
      return false;
    }

    // Check if there is a piece on field
    if (target !== 0) {
      console.log("Take piece");
      this.capturedPieces.push(target);
      this.paintCapturedPieces();
    }

    // Disable possibility to en passant after 1 move
    if (this.lastMovedPiece && this.lastMovedPiece.enPassant) {
      this.lastMovedPiece.enPassant = false;
    }

    // Define last moved piece (for en passant)
    this.lastMovedPiece = current;

    // Change board state
    this._fields[pos2[0]][pos2[1]] = current;
    this._fields[pos1[0]][pos1[1]] = 0;
    current.position = pos2;

    // Set correct defendefFields, checks and attackers
    this.checkDefendedFields();

    // Paint
    this.paintBoard();

    // Check for mates
    if (blackKing.isChecked && blackKing.isMate()) {
      alert("Mat, mat, mat... (Black king)");
      return false;
    }

    if (whiteKing.isChecked && whiteKing.isMate()) {
      alert("Mat, mat, mat... (White king)");
      return false;
    }

    // Check!
    if (blackKing.isChecked || whiteKing.isChecked) {
      alert("Check!");
    }

    // Pass turn
    this.passTurn();

    return true;
  };

  Board.prototype.setActivePiece = function(pos) {
    var x = pos[1],
      y = pos[0];

    var allCells = document.getElementsByClassName("cell");
    var rows = document.getElementsByClassName("row");
    var row = rows[y];
    var cells = row.getElementsByClassName("cell");
    var cell = cells[x];

    for (var i = allCells.length - 1; i >= 0; i--) {
      removeClass(allCells[i], "active");
    }

    addClass(cell, "active");

    activePiece = board._fields[y][x];
  };

  Board.prototype.addCellListeners = function() {
    var cells = document.querySelectorAll(".cell");

    cells.forEach(function(elem) {
      elem.addEventListener("click", function(event) {
        var cell = event.target;
        var x = Number(cell.dataset.x);
        var y = Number(cell.dataset.y);

        if (board.isLegalPiece([y, x])) {
          board.setActivePiece([y, x]);
        } else {
          if (activePiece) {
            activePiece.move([y, x]);
            activePiece = false;
          }
        }
      });
    });
  };

  function init() {
    console.log("Init board...");
    board = new Board();
  }

  return {
    init: init
  };
})();
