var ROOK = (function() {
  function Rook(color, pos) {
    this.color = color;
    this.position = pos;
    this.hasMoved = false;
    this.__name = "rook";
  }

  Rook.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];

    if (y > 0) {
      for (var i = y - 1; i >= 0; i--) {
        if (board.calculateDefendedField([i, x], this)) {
          break;
        }
      }
    }

    if (y < 7) {
      for (var i = y + 1; i <= 7; i++) {
        if (board.calculateDefendedField([i, x], this)) {
          break;
        }
      }
    }

    if (x > 0) {
      for (var i = x - 1; i >= 0; i--) {
        if (board.calculateDefendedField([y, i], this)) {
          break;
        }
      }
    }

    if (x < 7) {
      for (var i = x + 1; i <= 7; i++) {
        if (board.calculateDefendedField([y, i], this)) {
          break;
        }
      }
    }
  };

  Rook.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];

    // Illegal movedirection
    if (x1 !== x2 && y1 !== y2) {
      console.log("Illegal move direction!");
      return false;
    }

    // Detect collision
    if (x1 !== x2) {
      for (
        var i = x2 > x1 ? x1 + 1 : x1 - 1;
        x2 > x1 ? i < x2 - 1 : i > x2 + 1;
        x2 > x1 ? i++ : i--
      ) {
        if (board._fields[y1][i] !== 0) {
          console.log("Collision detected!");
          return false;
        }
      }
    }

    if (y1 !== y2) {
      for (
        var i = y2 > y1 ? y1 + 1 : y1 - 1;
        y2 > y1 ? i < y2 - 1 : i > y2 + 1;
        y2 > y1 ? i++ : i--
      ) {
        if (board._fields[i][x1] !== 0) {
          console.log("Collision detected!");
          return false;
        }
      }
    }

    // Just move
    if (board.move(this.position, pos)) {
      this.hasMoved = true;
    }
  };

  function init(color, pos) {
    console.log("Init rook...");
    var rook = new Rook(color, pos);
    return rook;
  }

  return {
    init: init
  };
})();
