var PAWN = (function() {
  function Pawn(color, pos) {
    this.color = color;
    this.position = pos;
    this.canMoveTwo = true;
    this.direction = this.color === "white" ? -1 : 1;
    this.enPassant = false;
    this.__name = "pawn";
  }

  Pawn.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];

    if (x > 0 && y > 0 && y < 7) {
      board.calculateDefendedField([y + 1 * this.direction, x - 1], this);
    }

    if (x < 7 && y > 0 && y < 7) {
      board.calculateDefendedField([y + 1 * this.direction, x + 1], this);
    }
  };

  Pawn.prototype.promote = function() {
    var pieces = ["Q", "R", "B", "Kn"];
    var choosePiece, newPiece;

    do {
      choosePiece = prompt("Which piece? (Q, R, B, Kn)");
    } while (pieces.indexOf(choosePiece) === -1);

    switch (choosePiece) {
      case "Q":
        newPiece = QUEEN.init(this.color, this.position);
        break;

      case "R":
        newPiece = ROOK.init(this.color, this.position);
        break;

      case "B":
        newPiece = BISHOP.init(this.color, this.position);
        break;

      case "Kn":
        newPiece = KNIGHT.init(this.color, this.position);
        break;

      default:
    }

    board._fields[this.position[0]][this.position[1]] = newPiece;
  };

  Pawn.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];

    // Move 2 fields
    if (this.canMoveTwo) {
      if (
        y2 === y1 + 2 * this.direction &&
        x2 === x1 &&
        board._fields[y2][x2] === 0 &&
        board._fields[y2 - 1 * this.direction][x2] === 0
      ) {
        if (board.move(this.position, pos)) {
          this.canMoveTwo = false;
          this.enPassant = true;
        }
      }
    }

    // Move 1 field
    if (
      y2 === y1 + 1 * this.direction &&
      x2 === x1 &&
      board._fields[y2][x2] === 0
    ) {
      if (board.checkChecks(this.position, pos)) {
        this.canMoveTwo = false;

        // Promote
        if (y2 === 7 || y2 === 0) {
          this.promote();
        }

        board.move(this.position, pos);
      }
    }

    // Take piece
    if (x2 === x1 + 1 || x2 == x1 - 1) {
      // Check diagonal movement
      if (y2 === y1 + 1 * this.direction) {
        // Check vertical movement
        var passingPawn = board._fields[y2 - 1 * this.direction][x2];

        if (passingPawn !== 0 && passingPawn.enPassant) {
          // En Passant
          if (board.move(this.position, pos)) {
            // If it is a legal move
            board.capturedPieces.push(passingPawn);
            board.paintCapturedPieces();
            board._fields[y2 - 1 * this.direction][x2] = 0;
            board.paintBoard();
            return true;
          }
        }

        if (board._fields[y2][x2] !== 0) {
          // If there is a piece
          if (board.checkChecks(this.position, pos)) {
            // If it is a legal move
            this.canMoveTwo = false;

            // Promote
            if (y2 === 7 || y2 === 0) {
              // If it gets to the end
              this.promote();
            }

            board.move(this.position, pos);
          }
        }
      }
    }
  };

  function init(color, pos) {
    console.log("Init pawn...");
    var pawn = new Pawn(color, pos);
    return pawn;
  }

  return {
    init: init
  };
})();
