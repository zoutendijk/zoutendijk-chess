var QUEEN = (function() {
  function Queen(color, pos) {
    this.color = color;
    this.position = pos;
    this.__name = "queen";
  }

  Queen.prototype.calculateDefendedFields = function() {
    var x = this.position[1],
      y = this.position[0];
    var currentX = x,
      currentY = y;

    if (x > 0 && y > 0) {
      // Top left
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY--;
        currentX--;

        if (currentY < 0 || currentX < 0) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x < 7 && y > 0) {
      // Top right
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY--;
        currentX++;

        if (currentY < 0 || currentX > 7) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x < 7 && y < 7) {
      // Bottom right
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY++;
        currentX++;

        if (currentY > 7 || currentX > 7) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (x > 0 && y < 7) {
      // Bottom left
      currentX = x;
      currentY = y;

      for (var i = 0; i <= 7; i++) {
        currentY++;
        currentX--;

        if (currentY > 7 || currentX < 0) {
          break;
        }
        if (board.calculateDefendedField([currentY, currentX], this)) {
          break;
        }
      }
    }

    if (y > 0) {
      for (var i = y - 1; i >= 0; i--) {
        if (board.calculateDefendedField([i, x], this)) {
          break;
        }
      }
    }

    if (y < 7) {
      for (var i = y + 1; i <= 7; i++) {
        if (board.calculateDefendedField([i, x], this)) {
          break;
        }
      }
    }

    if (x > 0) {
      for (var i = x - 1; i >= 0; i--) {
        if (board.calculateDefendedField([y, i], this)) {
          break;
        }
      }
    }

    if (x < 7) {
      for (var i = x + 1; i <= 7; i++) {
        if (board.calculateDefendedField([y, i], this)) {
          break;
        }
      }
    }
  };

  Queen.prototype.move = function(pos) {
    var x1 = this.position[1],
      y1 = this.position[0],
      x2 = pos[1],
      y2 = pos[0];

    // Diagonal movement
    if (x2 - x1 === y2 - y1 || x1 + y1 === x2 + y2) {
      // Detect collision for diagonals
      var range = x2 - x1;
      range = -range > 0 ? -range : range;

      if (range > 1) {
        var x = x1,
          y = y1;

        for (var i = 1; i < range; i++) {
          x = x2 > x1 ? x + 1 : x - 1;
          y = y2 > y1 ? y + 1 : y - 1;

          if (board._fields[y][x] !== 0) {
            console.log("Collision detected!");
            return false;
          }
        }
      }

      board.move(this.position, pos);
    }

    // Straight movement
    if (x1 === x2 || y1 === y2) {
      // Detect collision for columns and rows
      if (x1 !== x2) {
        for (
          var i = x2 > x1 ? x1 + 1 : x1 - 1;
          x2 > x1 ? i < x2 - 1 : i > x2 + 1;
          x2 > x1 ? i++ : i--
        ) {
          if (board._fields[y1][i] !== 0) {
            console.log("Collision detected!");
            return false;
          }
        }
      }

      if (y1 !== y2) {
        for (
          var i = y2 > y1 ? y1 + 1 : y1 - 1;
          y2 > y1 ? i < y2 - 1 : i > y2 + 1;
          y2 > y1 ? i++ : i--
        ) {
          if (board._fields[i][x1] !== 0) {
            console.log("Collision detected!");
            return false;
          }
        }
      }

      board.move(this.position, pos);
    }
  };

  function init(color, pos) {
    console.log("Init queen...");
    var queen = new Queen(color, pos);
    return queen;
  }

  return {
    init: init
  };
})();
