var gulp = require("gulp");
var sass = require("gulp-sass");
var plumber = require("gulp-plumber");
var concat = require("gulp-concat");
var uglify = require("gulp-uglify");
var autoprefixer = require("gulp-autoprefixer");
var cleanCSS = require("gulp-clean-css");
var browserSync = require("browser-sync").create();
var imagemin = require("gulp-imagemin");
var sourcemaps = require("gulp-sourcemaps");
var bulkSass = require("gulp-sass-bulk-import");

/* ==========================================================================
   IMAGES
   ========================================================================== */
gulp.task("images", function() {
  return gulp
    .src("images/*")
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo()
      ])
    )
    .pipe(gulp.dest("images/"));
});

/* ==========================================================================
   JS
   ========================================================================== */
gulp.task("js", function() {
  return (
    gulp
      .src([
        "js/modules/global.js",
        "js/**/*.js",
        "!js/dist/*.js",
        "!js/maps/*.js"
      ])
      .pipe(sourcemaps.init())
      .pipe(concat("all.min.js"))
      //.pipe(uglify())
      .pipe(sourcemaps.write("../maps"))
      .pipe(gulp.dest("js/dist"))
  );
});

gulp.task("js-watch", ["js"], function(done) {
  browserSync.reload();
  done();
});

/* ==========================================================================
   SASS
   ========================================================================== */
gulp.task("sass", function() {
  return gulp
    .src("sass/style.scss")
    .pipe(sourcemaps.init())
    .pipe(
      plumber(function(error) {
        gutil.log(error.message);
        this.emit("end");
      })
    )
    .pipe(bulkSass())
    .pipe(sass())
    .pipe(autoprefixer("last 4 version"))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write("./"))
    .pipe(gulp.dest("."))
    .pipe(browserSync.stream());
});

/* ==========================================================================
   DEFAULT
   ========================================================================== */
gulp.task("default", ["js"], function() {
  browserSync.init({
    proxy: "chess.localhost"
  });

  gulp.watch(["sass/**/*.scss"], ["sass"]);
  gulp.watch(["js/**/*.js", "!js/dist/*.js", "!js/maps/*.js"], ["js-watch"]);
  gulp.watch(["index.html"]).on("change", browserSync.reload);
  gulp.watch(["images/*.*"], ["images"]);
});
